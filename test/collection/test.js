class Test {
    /**
     * Runs the test.
     * 
     * @abstract 
     */
    static run(){
        console.error("Call of abstract method 'Test.run'. Overload in subclass.")
    }
}

module.exports = Test