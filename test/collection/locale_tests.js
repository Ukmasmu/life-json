let chai = require("chai")
const expect = chai.expect;
const Test = require("./test")

const Locale = require("../../lib/locale").LifeJSON.Locale

class LocaleTests extends Test {
    static run() {

        let library = null
        describe("Verify that library is excessible.", () => {
            it("Test If Library Is Excessible", () => {
                library = Locale.library
                expect(library).to.not.be.empty
            })
        })

        describe("Verify wrong translation calls are failing.", () => {
            const invalidTarget = "clingon"
            it("Verify invalid translation calls: " + invalidTarget, () => {
                let invalidLocale = new Locale(invalidTarget)
                let valid = true
                for (let key of Object.keys(Locale.library)) {
                    let result = invalidLocale.translate(Locale.library[key])
                    
                    if (result !== "") {
                        valid = false
                        break
                    }
                }

                expect(valid).to.be.true
            })
        })

        describe("Verify that translation calls are working.", () => {

            const validTarget = "eng"
            it("Verify valid translation call: " + validTarget, () => {
                let validLocale = new Locale(validTarget)
                let valid = true
                for (let key of Object.keys(Locale.library)) {
                    let result = validLocale.translate(Locale.library[key])
                    if (result == "") {
                        valid = false
                        break
                    }
                }
                expect(valid).to.be.true
            })
        })




    }
}

module.exports = LocaleTests