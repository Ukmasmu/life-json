const packageJSON = require("../../package.json")
let chai = require("chai")
chai.use(require("chai-fs"))
const expect = chai.expect;
const fs = require("fs")
const path = require("path")
const Test = require("./test")


class FileTests extends Test {

    static run() {

        const context = "../../"
        const langDirectory = "./lang"
        const truthFileName = "_template.json"
        let fileList = ["scheme.json"]

        describe("Verify Versions in Files", function () {
            const packageVersion = packageJSON.version
            console.log("Version of 'package.json': " + packageVersion)
            fileList.forEach(file => {
                const filepath = path.join(context, file)
                it("Test if file '" + filepath + "' has right version set.", function () {
                    const fileVersion = require(filepath).version
                    expect(fileVersion).to.be.equal(packageVersion)
                })
            })
        })



        describe("Verify that language files exist and have valid format.", function () {


            it("Test if language directory exists.", () => {
                expect(langDirectory).to.be.a.directory()
            })

            let files = fs.readdirSync(langDirectory)

            files.forEach((file) => {
                if ([".", ".."].indexOf(file) == -1) {
                    let filepath = path.join(langDirectory, file)
                    it("Test if language file is in JSON format: " + filepath + ".", () => {
                        expect(filepath).to.be.a.file().with.json;

                    })
                }
            })



        })

        describe("Verify that all langauge files are up to date with the template.", () => {

            it("Test if ground truth exists: " + path.join(langDirectory, truthFileName) + ".", () => {
                expect(path.join(langDirectory, truthFileName)).to.be.a.file();
            })

            it("Test at least one language is implemented", () => {

                let files = fs.readdirSync(langDirectory)

                let languageFiles = []

                files.forEach((file) => {
                    if ([".", "..", truthFileName].indexOf(file) == -1) {
                        languageFiles.push(file)
                    }
                })

                expect(languageFiles).to.not.be.empty
            })
        })


        describe("Verify that all languages meet the Templates implementation.", () => {

            let files = fs.readdirSync(langDirectory)
            let languageFiles = []
            files.forEach((file) => {
                it("Test if '" + path.join(langDirectory, file) + "' implements all keys.", () => {
                    const languageFile = require(path.join(context, langDirectory, file))
                    const templateFile = require(path.join(context, langDirectory, truthFileName))
                    expect(fileToHaveTheSameKeys(templateFile, languageFile)).to.be.true
                })
            })

        })

        function fileToHaveTheSameKeys(first, second) {
            let same = true
            let copy = Object.assign({}, second)
            for (let key of Object.keys(first)) {
                if (copy.hasOwnProperty(key)) {
                    delete copy[key]
                } else {
                    same = false
                    break
                }
            }
            return (same && Object.keys(copy).length == 0)
        }
    }
}

module.exports = FileTests