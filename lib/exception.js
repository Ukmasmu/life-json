class Exception {
    static LocaleNotImplemented(locale){
        return "Requested locale is not implemented yet: " + locale + "."
    }
}

module.exports.LifeJSON = module.exports.LifeJSON || {}
module.exports.LifeJSON.Exception = Exception