const Exception = require("./exception.js").LifeJSON.Exception

class Locale {

    /**
     * Constructs a Locale object for a provided locale.
     * 
     * @param {string} locale - String representation of a locale. It's supposed to be a three letter identifier specified in ISO 639.
     */
    constructor(locale) {
        this.locale = locale
    }

    translate(key) {
        let tralationFile = this.translationFile
        if (!tralationFile) {
            console.error(Exception.LocaleNotImplemented(this.locale))
            return ""
        } else {
            if (tralationFile[key])
                return tralationFile[key]
            else return ""
        }
    }

    get translationFile() {
        let file = {}
        try {
            file = require("../lang/" + this.locale + ".json")
        } catch (err) { }
        return file
    }

    static get library() {
        const template = require("../lang/_template.json")
        return Object.keys(template)
    }
}

module.exports.LifeJSON = module.exports.LifeJSON || {}
module.exports.LifeJSON.Locale = Locale