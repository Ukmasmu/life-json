# life-json

Life JSON is a JSON-Scheme for storing data about oneself's life like your job history. This format could be applied in many different scenarios, e.g. can be used to auto-build a CV.

## Languages

The languages folder is used for translating keys into a readable format of the respective language. They are named using the ISO 639.